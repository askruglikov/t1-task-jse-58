package ru.t1.kruglikov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.endpoint.*;
import ru.t1.kruglikov.tm.api.service.*;
import ru.t1.kruglikov.tm.api.service.dto.IProjectDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ISessionDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ITaskDtoService;
import ru.t1.kruglikov.tm.api.service.dto.IUserDtoService;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.endpoint.*;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.service.*;
import ru.t1.kruglikov.tm.service.dto.ProjectDtoService;
import ru.t1.kruglikov.tm.service.dto.SessionDtoService;
import ru.t1.kruglikov.tm.service.dto.TaskDtoService;
import ru.t1.kruglikov.tm.service.dto.UserDtoService;
import ru.t1.kruglikov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Component
public final class Bootstrap implements ILocatorService {

    @Autowired
    @NotNull
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IConnectionService connectionService;

    @Autowired
    @NotNull
    private IProjectDtoService projectService;

    @Autowired
    @NotNull
    private ITaskDtoService taskService;

    @Autowired
    @NotNull
    private IUserDtoService userService;

    @Autowired
    @NotNull
    private ISessionDtoService sessionService;

    @Autowired
    @NotNull
    private ILoggerService loggerService;

    @Autowired
    @NotNull
    private IDomainService domainService;

    @Autowired
    @NotNull
    private IAuthService authService;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private Backup backup;

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ':' + port + '/' + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        //initLogger();
        initDemoData();
        //initBackup();
        initJmsLogger();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final String adminLogin = "admin";
        @NotNull final String user1Login = "USER_01";
        @NotNull final String user2Login = "USER_02";
        @NotNull final String user3Login = "USER_03";

        @NotNull final UserDTO user1;
        @NotNull final UserDTO user2;
        @NotNull final UserDTO user3;

        if (!userService.isLoginExist(adminLogin)) userService.create(adminLogin, "admin", Role.ADMIN);
        if (!userService.isLoginExist(user1Login)) user1 = userService.create(user1Login, "user01");
        else user1 = userService.findByLogin(user1Login);
        if (!userService.isLoginExist(user2Login)) user2 = userService.create(user2Login, "user02", "user02@address.ru");
        else user2 = userService.findByLogin(user2Login);
        if (!userService.isLoginExist(user3Login)) user3 = userService.create(user3Login, "user03", "user03@address.ru");
        else user3 = userService.findByLogin(user3Login);

        projectService.create(user1.getId(), "PROJECT_01", "Test project 1");
        projectService.create(user1.getId(), "PROJECT_18", "Test project 18");
        projectService.create(user2.getId(), "PROJECT_02", "Test project 2");
        projectService.create(user3.getId(), "PROJECT_26", "Test project 26");

        taskService.create(user1.getId(), "TASK_01", "Test task 1");
        taskService.create(user1.getId(), "TASK_18", "Test task 18");
        taskService.create(user1.getId(), "TASK_02", "Test task 2");
        taskService.create(user2.getId(), "TASK_26", "Test task 26");
    }

    private void initBackup() {
        backup.init();
    }

    private void initJmsLogger() {
        loggerService.initJmsLogger();
    }

}
