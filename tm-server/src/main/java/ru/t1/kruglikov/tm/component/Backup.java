package ru.t1.kruglikov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Backup extends Thread {

    @NotNull
    @Autowired
    private final ILocatorService locatorService;

    public Backup(@NotNull final ILocatorService locatorService) {
        this.locatorService = locatorService;
        this.setDaemon(true);
    }

    public void init() {
        start();
    }

    public void save() {
        locatorService.getDomainService().dataBackupSave();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        locatorService.getDomainService().dataBackupLoad();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }

}

